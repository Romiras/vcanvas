MODULE VgTypesPriv;

	IMPORT
		Models := VgModels;
		
	CONST
		STATE_OPERATOR_DEFAULT* = Models.OPERATOR_OVER;
		STATE_TOLERANCE_DEFAULT* = 0.1;
		STATE_FILL_RULE_DEFAULT* = Models.FILL_RULE_WINDING;
		STATE_LINE_WIDTH_DEFAULT* = 2.0;
		STATE_LINE_CAP_DEFAULT* = Models.LINE_CAP_BUTT;
		STATE_LINE_JOIN_DEFAULT* = Models.LINE_JOIN_MITER;
		STATE_MITER_LIMIT_DEFAULT* = 10.0;
		STATE_DEFAULT_FONT_SIZE* = 10.0;
		FILTER_DEFAULT* = Models.FILTER_GOOD;
		EXTEND_SURFACE_DEFAULT* = Models.EXTEND_NONE;
		EXTEND_GRADIENT_DEFAULT* = Models.EXTEND_PAD;
	
		PATH_OP_CLOSE_PATH* = 0;
		PATH_OP_MOVE_TO* = 1;
		PATH_OP_LINE_TO* = 2;
		PATH_OP_CURVE_TO* = 3;

		(* PathBufSIZE = (512 - 4 * SIZE(INTEGER) - SIZE(PathBuf)) DIV (2 * SIZE(Point) + SIZE(PathOp)) *)
		PathBufSIZE* = 15;
	
	TYPE
		(* Dynamic types *)
		
		Element* = RECORD END;
		Pointer* = POINTER TO RECORD END;
		
		Array* = RECORD (* _cairo_array *)
			size, numElements, elementSize: INTEGER;
			isSnapshot: BOOLEAN;
			elements: POINTER TO ARRAY OF Element
		END;

		(* Base types *)
		
		Point = Models.Point;
		(*
		(* cairo_point_t *)
		Point* = RECORD
			x*, y*: INTEGER
		END;
		*)
		
		(* _cairo_point_double *)
		PointDouble* = RECORD
			x*, y*: REAL
		END;
		
		(* _cairo_distance_double *)
		DistanceDouble* = RECORD
			dx*, dy*: REAL
		END;
		
		(* _cairo_slope *)
		Slope* = RECORD
			dx*, dy*: INTEGER
		END;
		
		(* _cairo_line *)
		Line* = RECORD
			p1*, p2*: Point
		END;
		
		(* _cairo_trapezoid *)
		Trapezoid* = RECORD
			top*, bottom*: Point;
			left*, right*: Line
		END;
		
		(* cairo_rectangle_int_t *)
		Rectangle* = RECORD
			x*, y*,
			width*, height*: INTEGER
		END;
		
		(* cairo_box_int_t *)
		Box* = RECORD
			p1*, p2*: Point
		END;
		
		(* cairo_region_t, pixman_region16 *)
		Region* = RECORD
			(*
			pixman_box16_t          extents;
			pixman_region16_data_t *data;
			*)
			extents*: Rectangle;
			size*, numRects*: INTEGER;
			data*: POINTER TO ARRAY OF REAL (* rects[size] *)
		END;
		
		(* cairo_edge_t *)
		Edge* = RECORD
			edge*: Line;
			clockWise*,
			currentX*: INTEGER;
		END;
		
		(* cairo_polygon_t *)
		Polygon* = RECORD
			(*status: Status;*)
			firstPoint*,
			currentPoint*: Point;
			hasCurrentPoint*: BOOLEAN;
			num_edges*,
			edges_size*: INTEGER;
			edges*: POINTER TO ARRAY OF Edge;
			edgesEmbedded*: ARRAY 8 OF Edge
		END;
		
		(* cairo_spline_knots_t *)
		SplineKnots* = RECORD
			a*, b*, c*, d*: Point
		END;
		
		(* cairo_spline_t *)
		Spline* = RECORD
			knots*: SplineKnots;
			initial_slope*,
			final_slope*: Slope;
			num_points*,
			points_size*: INTEGER;
			points*: POINTER TO ARRAY OF Point;
			points_embedded*: ARRAY 8 OF Point
		END;
		
		(* cairo_pen_vertex_t *)
		PenVertex* = RECORD
			point*: Point;
			slope_ccw*,
			slope_cw*: Slope
		END;
		
		(* cairo_pen_t *)
		Pen* = RECORD
			radius*,
			tolerance*: REAL;
			num_vertices*: INTEGER;
			vertices*: POINTER TO ARRAY OF PenVertex
		END;
		
		(* cairo_path_data_type_t *)
		PathPoint* = Point;

		(* cairo_path_data_t *)
		PathHeader* = RECORD
			type*: Models.PathType;
			length*: INTEGER
		END;
		
		(* cairo_path_data_t *)
		PathData* = POINTER TO ARRAY OF RECORD
			header*: PathHeader;
			points*: POINTER TO ARRAY OF PathPoint
		END;
		
		(*
		(* _cairo_path_buf *)
		PathBuf* = RECORD
			next*,
			prev*: POINTER TO PathBuf;
			bufSize* : INTEGER;
			numOps* : INTEGER;
			numPoints* : INTEGER;
			op* : POINTER TO ARRAY OF PathOp;
			points* : POINTER TO ARRAY OF Point
		END;
		
		(* _cairo_path_buf_fixed *)
		PathBufFixed* = POINTER TO RECORD
			base* : PathBuf;
			op* : ARRAY PathBufSIZE OF PathOp;
			points* : ARRAY 2*PathBufSIZE OF Point;
		END;
		*)
		
		PathBuf* = POINTER TO RECORD
			next*,
			prev*: PathBuf;
			bufSize* : INTEGER;
			numOps* : INTEGER;
			numPoints* : INTEGER;
			op* : POINTER TO ARRAY OF Models.PathOp;
			points* : POINTER TO ARRAY OF Point
		END;
		
		(* _cairo_path_fixed *)
		PathFixed* = RECORD
			last_move_point*,
			current_point* : Point;
			opts* : SET; (* has_current_point, has_curve_to *)
			buf_head*,
			buf_tail* : PathBuf
		END;
		
		(* cairo_clip_mode_t *)
		ClipMode* = INTEGER;

		(* _cairo_clip_path *)
		ClipPath* = POINTER TO RECORD
			path*: PathFixed;
			fillRule*: Models.FillRule;
			tolerance*: REAL;
			prev*: ClipPath
		END;
		
		(* cairo_direction_t *)
		Direction* = INTEGER;
		
		GlyphSize* = Rectangle;
		
		(* cairo_traps_t *)
		Traps* = RECORD
			(*status*: Status;*)
			extents*: Box;
			num_traps*,
			traps_size*: INTEGER;
			traps*: POINTER TO ARRAY OF Trapezoid;
			traps_embedded*: ARRAY 1 OF Trapezoid;
			has_limits*: BOOLEAN;
			limits*: Box
		END;
		
		(* cairo_format_masks_t *)
		FormatMasks* = RECORD
			bpp*,
			alpha_mask*,
			red_mask*,
			green_mask*,
			blue_mask*: INTEGER
		END;
		
		(* cairo_stock_t *)
		Stock* = INTEGER;
		
		(* cairo_image_transparency_t *)
		ImageTransparency* = INTEGER;
		
		Clip* = RECORD (* _cairo_clip *)
			mode* : ClipMode;
			all_clipped*,
			has_region* : BOOLEAN;
			(*view* : Views.View;*)
			viewmap* : Rectangle;
			serial* : INTEGER;
			region* : Region;
			path* : ClipPath
		END;
		
		ModelAttributes* = RECORD
			source* : Models.Pattern;
			strokeStyle* : Models.StrokeStyle;
			fillRule* : Models.FillRule;
			tolerance* : REAL;
			operator* : Models.Operator;
			clip* : Clip;
			(*
			fontFace* : FontFace; (* ptr *)
			scaledFont* : ScaledFont; (* ptr *)
			fontMatrix* : Models.AffineMatrix;
			fontOptions* : Models.FontOptions;
			*)
			ctm* : Models.AffineMatrix;
		END;
		
		ModelState* = POINTER TO RECORD (Models.Store)
			attr* : ModelAttributes;
			next* : ModelState
		END;

END VgTypesPriv.
