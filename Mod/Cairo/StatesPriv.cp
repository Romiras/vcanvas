MODULE VgStatesPriv;

	IMPORT
		Models := VgModels, TypesPriv := VgTypesPriv, Utils := VgUtils;
	
	PROCEDURE StrokeStyleInit (VAR strokeStyle: Models.StrokeStyle);
	BEGIN
		strokeStyle.lineWidth := TypesPriv.STATE_LINE_WIDTH_DEFAULT;
		strokeStyle.lineCap := TypesPriv.STATE_LINE_CAP_DEFAULT;
		strokeStyle.lineJoin := TypesPriv.STATE_LINE_JOIN_DEFAULT;
		strokeStyle.numDashes := 0;
		strokeStyle.dash := NIL;
		strokeStyle.dashOffset := 0.0;
		strokeStyle.miterLimit := 0
	END StrokeStyleInit;
	
	PROCEDURE ClipInit (VAR clip: TypesPriv.Clip);
	BEGIN
	END ClipInit;
	
	PROCEDURE StateInit* (state: TypesPriv.ModelState);
	BEGIN
		state.next := NIL;
		state.attr.source := NIL;
		StrokeStyleInit (state.attr.strokeStyle);
		state.attr.fillRule := TypesPriv.STATE_FILL_RULE_DEFAULT;
		state.attr.tolerance := TypesPriv.STATE_TOLERANCE_DEFAULT;
		state.attr.operator := TypesPriv.STATE_OPERATOR_DEFAULT;
		ClipInit (state.attr.clip);
		Utils.MatrixInitIdentity (state.attr.ctm)
	END StateInit;
	
	PROCEDURE StateFini* (state: TypesPriv.ModelState);
	BEGIN
		state.next := NIL;
		state.attr.source := NIL;
		StrokeStyleInit (state.attr.strokeStyle);
		state.attr.fillRule := TypesPriv.STATE_FILL_RULE_DEFAULT;
		state.attr.tolerance := TypesPriv.STATE_TOLERANCE_DEFAULT;
		state.attr.operator := TypesPriv.STATE_OPERATOR_DEFAULT;
		ClipInit (state.attr.clip);
		Utils.MatrixInitIdentity (state.attr.ctm)
	END StateFini;
	
	PROCEDURE StateCopy (dest, src: TypesPriv.ModelState);
	BEGIN
		dest.next := NIL;
		dest.attr.source := src.attr.source;
		dest.attr.strokeStyle := src.attr.strokeStyle;
		dest.attr.fillRule := src.attr.fillRule;
		dest.attr.tolerance := src.attr.tolerance;
		dest.attr.operator := src.attr.operator;
		dest.attr.clip := src.attr.clip;
		dest.attr.ctm := src.attr.ctm
	END StateCopy;
	
	PROCEDURE StateClone* (state: TypesPriv.ModelState; OUT out: TypesPriv.ModelState);
	BEGIN
		NEW(out);
		IF out # NIL THEN
			StateCopy (state, out);
			Models.SetStatus(Models.STATUS_SUCCESS)
		ELSE
			Models.SetStatus(Models.STATUS_NO_MEMORY)
		END
	END StateClone;
	
	PROCEDURE StateSave* (state: TypesPriv.ModelState);
		VAR top: TypesPriv.ModelState;
	BEGIN
		top := NIL;
		StateClone (state, top);
		IF Models.status = Models.STATUS_SUCCESS THEN
			top.next := state; state := top
		END
	END StateSave;
	
	PROCEDURE StateRestore* (state: TypesPriv.ModelState);
		VAR top: TypesPriv.ModelState;
	BEGIN
		top := state;
		IF top.next # NIL THEN
			state := top.next; top := NIL;
			Models.SetStatus(Models.STATUS_SUCCESS)
		ELSE
			Models.SetStatus(Models.STATUS_INVALID_RESTORE)
		END
	END StateRestore;
	
	PROCEDURE Translate* (state: TypesPriv.ModelState; tx, ty: REAL);
		VAR tmp: Models.AffineMatrix;
	BEGIN
		IF Utils.IsFinite(tx) & Utils.IsFinite(ty) THEN
			Utils.MatrixInitTranslate(tmp, tx, ty);
			Utils.MatrixMult(state.attr.ctm, tmp, state.attr.ctm);
			Models.SetStatus(Models.STATUS_SUCCESS)
		ELSE
			Models.SetStatus(Models.STATUS_INVALID_MATRIX)
		END
	END Translate;
	
	PROCEDURE Scale* (state: TypesPriv.ModelState; sx, sy: REAL);
		VAR tmp: Models.AffineMatrix;
	BEGIN
		IF sx * sy # 0.0 THEN
			Utils.MatrixInitScale(tmp, sx, sy);
			Utils.MatrixMult(state.attr.ctm, tmp, state.attr.ctm);
			Models.SetStatus(Models.STATUS_SUCCESS)
		ELSE
			Models.SetStatus(Models.STATUS_INVALID_MATRIX)
		END
	END Scale;
	
	PROCEDURE Rotate* (state: TypesPriv.ModelState; angle: REAL);
		VAR tmp: Models.AffineMatrix;
	BEGIN
		IF angle = 0.0 THEN Models.SetStatus(Models.STATUS_SUCCESS); RETURN END;
		Utils.MatrixInitRotate(tmp, angle);
		Utils.MatrixMult(state.attr.ctm, tmp, state.attr.ctm);
		Models.SetStatus(Models.STATUS_SUCCESS)
	END Rotate;
	
	PROCEDURE Transform* (state: TypesPriv.ModelState; matrix: Models.AffineMatrix);
	BEGIN
		(* ctm := matrix * ctm *)
		Utils.MatrixMult(state.attr.ctm, matrix, state.attr.ctm)
	END Transform;

END VgStatesPriv.