MODULE VgUtils;

	IMPORT Math, Models := VgModels;
	
	PROCEDURE IsFinite* (v: REAL): BOOLEAN;
	BEGIN
		RETURN v * v > 0.0 (* check for NaNs *)
	END IsFinite;
	
	PROCEDURE RestrictValue* (VAR val: REAL; min, max: REAL);
	BEGIN
		IF val < min THEN val := min END;
		IF val > max THEN val := max END
	END RestrictValue;

	PROCEDURE MatrixInit*(OUT matrix: Models.AffineMatrix;
		xx, xy,
		yx, yy,
		tx, ty: REAL);
	BEGIN
		matrix.tf[0, 0] := xx; matrix.tf[0, 1] := xy;
		matrix.tf[1, 0] := yx; matrix.tf[1, 1] := yy;
		matrix.tl[0, 0] := tx;
		matrix.tl[1, 0] := ty
	END MatrixInit;
	
	PROCEDURE MatrixInitIdentity*(OUT matrix: Models.AffineMatrix);
	BEGIN
		MatrixInit(matrix,
			1.0, 0.0,
			0.0, 1.0,
			0.0, 0.0)
	END MatrixInitIdentity;
	
	PROCEDURE MatrixInitTranslate*(OUT matrix: Models.AffineMatrix; tx, ty: REAL);
	BEGIN
		MatrixInit(matrix,
			1.0, 0.0,
			0.0, 1.0,
			tx, ty)
	END MatrixInitTranslate;
	
	PROCEDURE MatrixInitScale*(OUT matrix: Models.AffineMatrix; sx, sy: REAL);
	BEGIN
		MatrixInit(matrix,
			sx, 0.0,
			0.0, sy,
			0.0, 0.0)
	END MatrixInitScale;
	
	PROCEDURE MatrixInitRotate*(OUT matrix: Models.AffineMatrix; angle: REAL);
		VAR sin, cos: REAL;
	BEGIN
		sin := Math.Sin(angle);
		cos := Math.Cos(angle);
		MatrixInit(matrix,
			cos, sin,
			-sin, cos,
			0.0, 0.0)
	END MatrixInitRotate;
	
	PROCEDURE MatrixMult*(OUT C: Models.AffineMatrix; A, B: Models.AffineMatrix);
	BEGIN
		C.tf[0, 0] := A.tf[0, 0] * B.tf[0, 0] + A.tf[1, 0] * B.tf[0, 1];
		C.tf[1, 0] := A.tf[0, 0] * B.tf[1, 0] + A.tf[1, 0] * B.tf[1, 1];
		
		C.tf[0, 1] := A.tf[0, 1] * B.tf[0, 0] + A.tf[1, 1] * B.tf[0, 1];
		C.tf[1, 1] := A.tf[0, 1] * B.tf[1, 0] + A.tf[1, 1] * B.tf[1, 1];
		
		C.tl[0, 0] := A.tl[0, 0] * B.tf[0, 0] + A.tl[1, 0] * B.tf[0, 1] + B.tl[0, 0];
		C.tl[1, 0] := A.tl[0, 0] * B.tf[1, 0] + A.tl[1, 0] * B.tf[1, 1] + B.tl[1, 0]
	END MatrixMult;
	
	(* cairo_matrix_transform_distance *)
	PROCEDURE MatrixTranslateDist*(matrix: Models.AffineMatrix; OUT dx, dy: REAL);
		VAR xNew, yNew: REAL;
	BEGIN
		xNew := matrix.tf[0, 0] * dx + matrix.tf[0, 1] * dy;
		yNew := matrix.tf[1, 0] * dx + matrix.tf[1, 1] * dy;
		dx := xNew;
		dy := yNew
	END MatrixTranslateDist;

	(* cairo_matrix_transform_point *)
	PROCEDURE MatrixTranslateCoord*(matrix: Models.AffineMatrix; OUT dx, dy: REAL);
		VAR xNew, yNew: REAL;
	BEGIN
		xNew := matrix.tf[0, 0] * dx + matrix.tf[0, 1] * dy;
		yNew := matrix.tf[1, 0] * dx + matrix.tf[1, 1] * dy;
		dx := xNew;
		dy := yNew
	END MatrixTranslateCoord;
	
	PROCEDURE MatrixScalarMult*(OUT matrix: Models.AffineMatrix; scalar: REAL);
	BEGIN
		matrix.tf[0, 0] := matrix.tf[0, 0] * scalar;
		matrix.tf[1, 0] := matrix.tf[1, 0] * scalar;
		
		matrix.tf[0, 1] := matrix.tf[0, 1] * scalar;
		matrix.tf[1, 1] := matrix.tf[1, 1] * scalar;
		
		matrix.tl[0, 0] := matrix.tl[0, 0] * scalar;
		matrix.tl[1, 0] := matrix.tl[1, 0] * scalar
	END MatrixScalarMult;
	
	PROCEDURE MatrixDet*(matrix: Models.AffineMatrix; OUT det: REAL);
	BEGIN
		det := matrix.tf[0, 0] * matrix.tf[1, 1] - matrix.tf[0, 1] * matrix.tf[1, 0]
	END MatrixDet;
	
	PROCEDURE MatrixIsInvertible*(matrix: Models.AffineMatrix): BOOLEAN;
		VAR det: REAL;
	BEGIN
		MatrixDet(matrix, det);
		RETURN (det # 0) & IsFinite(det)
	END MatrixIsInvertible;
	
	PROCEDURE MatrixAdjoint (OUT matrix: Models.AffineMatrix);
		VAR a, b, c, d, tx, ty: REAL;
	BEGIN
		a := matrix.tf[0, 0]; b := matrix.tf[0, 1];
		c := matrix.tf[1, 0]; d := matrix.tf[1, 1];
		tx := matrix.tl[0, 0]; ty := matrix.tl[1, 0];
		MatrixInit(matrix,
			d, -b,
			-c, a,
			c * ty - d * tx, b * tx - a * ty)
	END MatrixAdjoint;
	
	PROCEDURE MatrixInvert*(OUT matrix: Models.AffineMatrix);
		VAR det: REAL;
	BEGIN
		MatrixDet(matrix, det);
		IF MatrixIsInvertible(matrix) THEN
			MatrixAdjoint(matrix);
			MatrixScalarMult(matrix, 1.0 / det);
		ELSE
			Models.SetStatus(Models.STATUS_INVALID_MATRIX)
		END
	END MatrixInvert;
	
END VgUtils.