MODULE VgMappers;
	
	IMPORT Models := VgModels;
	
	TYPE		
		PathFormatter* = RECORD
			rider-: Models.Path (* Writer *)
		END;

	(* PathFormatter *)

	PROCEDURE (VAR f: PathFormatter) ConnectTo* (model: Models.Model), NEW;
	BEGIN
		IF model # NIL THEN
			f.rider := model.NewPathWriter(f.rider)
		ELSE
			f.rider := NIL
		END
	END ConnectTo;
	
	PROCEDURE (VAR f: PathFormatter) NewPath*, NEW;
	BEGIN
		f.rider.NewPath
	END NewPath;

	PROCEDURE (VAR f: PathFormatter) NewSubPath*, NEW;
	BEGIN
		f.rider.NewSubPath
	END NewSubPath;

	PROCEDURE (VAR f: PathFormatter) ClosePath*, NEW;
	BEGIN
		f.rider.ClosePath
	END ClosePath;

	PROCEDURE (VAR f: PathFormatter) AppendPath* (path: Models.Path), NEW;
	BEGIN
		f.rider.AppendPath(path)
	END AppendPath;

	PROCEDURE (VAR f: PathFormatter) CopyPath* (): Models.Path, NEW;
		VAR w: Models.Path;
	BEGIN
		w := f.rider.CopyPath();
		RETURN w
	END CopyPath;

	PROCEDURE (VAR f: PathFormatter) MoveTo* (x, y: REAL; mode: INTEGER), NEW;
	BEGIN
		f.rider.MoveTo(x, y, mode)
	END MoveTo;

	(*PROCEDURE (VAR f: PathFormatter) MoveToRel* (dx, dy: REAL), NEW;
	BEGIN
		f.rider.MoveToRel(dx, dy)
	END MoveToRel;*)

	PROCEDURE (VAR f: PathFormatter) GetCurrentPoint* (OUT x, y: REAL), NEW;
	BEGIN
		f.rider.GetCurrentPoint(x, y)
	END GetCurrentPoint;

	PROCEDURE (VAR f: PathFormatter) LineTo*(x, y: REAL; mode: INTEGER), NEW;
	BEGIN
		f.rider.LineTo(x, y, mode)
	END LineTo;

	(*PROCEDURE (VAR f: PathFormatter) LineToRel*(dx, dy: REAL), NEW;
	BEGIN
		f.rider.LineToRel(dx, dy)
	END LineToRel;*)

	PROCEDURE (VAR f: PathFormatter) Rectangle*(x, y, width, height: REAL), NEW;
	BEGIN
		(*f.rider.*)
	END Rectangle;

	PROCEDURE (VAR f: PathFormatter) CurveTo*(x1, y1, x2, y2, x3, y3: REAL; mode: INTEGER), NEW;
	BEGIN
		f.rider.CurveTo(x1, y1, x2, y2, x3, y3, mode)
	END CurveTo;

	(*PROCEDURE (VAR f: PathFormatter) CurveToRel*(dx1, dy1, dx2, dy2, dx3, dy3: REAL), NEW;
	BEGIN
		f.rider.CurveToRel(dx1, dy1, dx2, dy2, dx3, dy3)
	END CurveToRel;*)

	PROCEDURE (VAR f: PathFormatter) Arc*(xc, yc, radius, angle1, angle2: REAL), NEW;
	BEGIN
		(*f.rider.*)
	END Arc;

	PROCEDURE (VAR f: PathFormatter) ArcNegative*(xc, yc, radius, angle1, angle2: REAL), NEW;
	BEGIN
		(*f.rider.*)
	END ArcNegative;
	
END VgMappers.