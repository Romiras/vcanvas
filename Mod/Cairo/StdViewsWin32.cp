MODULE VgStdViews;

	(* Implementation by Cairo graphics library *)

	IMPORT
		Models := VgModels, Views := VgViews,
		LibsCairoWin32, WinApi;
	
	TYPE
		Win32Surface* = POINTER TO RECORD (Views.DrawableSurface)
			hdc: WinApi.HANDLE;
		END;

	PROCEDURE (s: Win32Surface) Create* (hdc: WinApi.HANDLE), NEW;
	BEGIN
		s.hdc := hdc;
		s.surface := LibsCairoWin32.cairo_win32_surface_create(hdc)
	END Create;
	
	PROCEDURE NewWin32Surface* (hdc: WinApi.HANDLE): Win32Surface;
		VAR s: Win32Surface;
	BEGIN
		NEW(s); s.Create(hdc);
		RETURN NIL
	END NewWin32Surface;
	
END VgStdViews.