MODULE VgColorspaces;

	TYPE
		ColorDesc* = ABSTRACT RECORD END;
		
		(*
		Color consists of N components, each of it has
			mathematical description: real number in range 0..1 - REAL
			machine representation: whole number in range 0..255 - BYTE
		*)
		
		RGBAColorComponents* = ARRAY 4 OF BYTE;
				
		RGBAColorDesc* = RECORD (ColorDesc)
			components-: RGBAColorComponents
		END;
		
	PROCEDURE Normalise (c: BYTE): REAL;
	BEGIN
		(* c is 0..255 *)
		RETURN c / 255.0
	END Normalise;
	
	PROCEDURE Denormalise (channel: REAL): BYTE;
	BEGIN
		(* channel is normalised to 0..1 like in OpenGL *)
		RETURN SHORT(SHORT(SHORT(ENTIER(channel * 255.0))))
	END Denormalise;
	
	(**
		Sets R, G, B, A values to ColorComponents
		r - red
		g - green
		b - blue
		a - transparency
	*)
	PROCEDURE (VAR C: RGBAColorDesc) SetColor* (r, g, b, a: REAL), NEW;
	BEGIN
		C.components[0] := Denormalise(r);
		C.components[1] := Denormalise(g);
		C.components[2] := Denormalise(b);
		C.components[3] := Denormalise(a);
	END SetColor;
	
	PROCEDURE (VAR C: RGBAColorDesc) GetColor* (OUT r, g, b, a: REAL), NEW;
	BEGIN
		r := Normalise(C.components[0]);
		g := Normalise(C.components[1]);
		b := Normalise(C.components[2]);
		a := Normalise(C.components[3]);
	END GetColor;
	
END VgColorspaces.
