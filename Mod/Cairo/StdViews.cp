MODULE VgStdViews;

	(* Implementation by Cairo graphics library *)
	
	IMPORT Models := VgModels, Views := VgViews, TypesPriv := VgTypesPriv,
		Cairo := LibsCairo, (*CairoWin32 := LibsCairoWin32, WinApi,*)
		Log := StdLog;
		
	TYPE
		
		(* Implementation *)
		
		(*
		(* _cairo_surface_backend *)
		SurfaceBackend = RECORD
		END;
		*)
		
		ViewAttr = RECORD (* cairo_surface_attributes_t *)
			matrix: Models.AffineMatrix;
			extend: Models.PatternExtend;
			filter: Models.RasterFilter;
			xOffset,
			yOffset: INTEGER;
			acquired: BOOLEAN;
			(*extra: Pointer *)
		END;
		
		StdSurface = POINTER TO ABSTRACT RECORD (Views.Surface)
			model: Models.Model;
			pattern: Cairo.Pcairo_pattern_t;
			surface: Cairo.Pcairo_surface_t;
			cr: Cairo.Pcairo_t;
			(*
			backend: SurfaceBackend;
			finished,
			is_snapshot,
			has_font_options: BOOLEAN;
			(*userData: Array;*)
			(* (* image-based fallback *)
			x_fallback_resolution,
			y_fallback_resolution: REAL;*)
			clip: TypesPriv.Clip;
			next_clip_serial,
			current_clip_serial: INTEGER
			*)
		END;
		
		StdDirectory = POINTER TO RECORD (Views.Directory) END;
		
		(*
		Win32Surface* = POINTER TO RECORD (DrawableSurface)
			hdc: WinApi.HANDLE;
		END;
		*)
	
	(* See Math.Floor() *)
	PROCEDURE Entier (r: REAL): INTEGER;
	BEGIN
		RETURN SHORT(ENTIER(r))
	END Entier;
	
	PROCEDURE StdInit (t: StdSurface);
	BEGIN
		ASSERT(t # NIL, 20);
		t.cr := Cairo.cairo_create(t.surface);
	END StdInit;
	
	
	PROCEDURE (s: Views.ImageSurface) Create* (format: Views.ImageFormat);
	BEGIN
		s.format := format;
		s.surface := Cairo.cairo_image_surface_create(
			format,
			Entier(s.model.width),
			Entier(s.model.height));
	END Create;
	
	
	PROCEDURE (S: StdSurface) Destroy;
		VAR
			status: Cairo.cairo_status_t;
	BEGIN
		status := Cairo.cairo_surface_write_to_png(S.surface, "d:\vg_image.png");
		Cairo.cairo_pattern_destroy(S.pattern);
		Cairo.cairo_destroy(S.cr);
		Cairo.cairo_surface_destroy(S.surface)
	END Destroy;
	
	PROCEDURE PathAdd (surface: StdSurface; Num: INTEGER; points: POINTER TO ARRAY OF Models.Point;
		ops: POINTER TO ARRAY OF Models.PathOp);
		VAR i, op: INTEGER;
	BEGIN
		i := 0;
		WHILE i < Num DO
			op := ops[i];
			CASE ops[i] OF
				TypesPriv.PATH_OP_CLOSE_PATH:
					Cairo.cairo_close_path (surface.cr);
					INC(i)
				| TypesPriv.PATH_OP_MOVE_TO:
					Cairo.cairo_move_to (surface.cr, points[i].x, points[i].y);
					INC(i)
				| TypesPriv.PATH_OP_LINE_TO:
					Cairo.cairo_line_to (surface.cr, points[i].x, points[i].y);
					INC(i)
				| TypesPriv.PATH_OP_CURVE_TO:
					Cairo.cairo_curve_to (surface.cr,
						points[i].x, points[i].y,
						points[i + 1].x, points[i + 1].y,
						points[i + 2].x, points[i + 2].y);
					INC(i, 3)
			END
		END
	END PathAdd;
	
	PROCEDURE SetSource(S: StdSurface);
		VAR pattern: Models.Pattern;
			spattern: StdModels.SolidPattern;
			r, g, b,
			x0, y0, x1, y1,
			rad1, rad2: REAL;
	BEGIN
		pattern := S.model.GetSource();
		IF pattern # NIL THEN
			WITH
				pattern: StdModels.SolidPattern DO
					pattern.GetColorRGB (r, g, b);
					Cairo.cairo_set_source_rgb(S.cr, r, g, b)
				| pattern: StdModels.LinearPattern DO
					S.pattern := Cairo.cairo_pattern_create_linear(
						pattern.p1.x, pattern.p1.y,
						pattern.p2.x, pattern.p2.y);
					Cairo.cairo_set_source (S.cr, S.pattern)
				| pattern: StdModels.RadialPattern DO
					S.pattern := Cairo.cairo_pattern_create_radial(
						pattern.c1.x, pattern.c1.y, pattern.r1,
						pattern.c2.x, pattern.c2.y, pattern.r2);
					Cairo.cairo_set_source (S.cr, S.pattern)
			END
		END
	END SetSource;
	
	PROCEDURE Render(S: StdSurface);
		VAR
			points: POINTER TO ARRAY OF Models.Point;
			ops: POINTER TO ARRAY OF Models.PathOp;
	BEGIN
		(* Render paths *)
		IF S.model.PathGetFirst() THEN
			S.model.PathGetPoints(points);
			S.model.PathGetOp(ops);
			PathAdd (S, S.model.PathGetNumPoints(), points, ops);
			WHILE S.model.PathGetNext() DO
				S.model.PathGetPoints(points);
				S.model.PathGetOp(ops);
				PathAdd (S, S.model.PathGetNumPoints(), points, ops)
			END
		END;
	END Render;
	
	PROCEDURE (S: StdSurface) Stroke;
	BEGIN
		(* Setting source *)
		SetSource(S);
		
		Render(S);
		Cairo.cairo_stroke(S.cr)
	END Stroke;
	
	PROCEDURE (S: StdSurface) Fill;
	BEGIN
		(* Setting source *)
		SetSource(S);
		
		Render(S);
		Cairo.cairo_fill(S.cr)
	END Fill;
	
	(*
	PROCEDURE (S: StdSurface) Flush;
	BEGIN
	END Flush;
	
	PROCEDURE (S: StdSurface) MarkDirty;
	BEGIN
	END MarkDirty;
	
	PROCEDURE (S: StdSurface) Paint;
	BEGIN
	END Paint;
	*)
	
	(* StdDirectory *)
	
	PROCEDURE (d: StdDirectory) NewImageSurface (mdl: Models.Model; format: Models.ImageFormat): ImageSurface;
		VAR t: ImageSurface;
	BEGIN
		NEW(t); t.InitModel(mdl); t.Create(format); StdInit(t);
		RETURN t
	END NewImageSurface;
	
	(*
	PROCEDURE (d: StdDirectory) NewDocumentSurface (mdl: Models.Model; filename: ARRAY OF CHAR): DocumentSurface, NEW;
		VAR t: DocumentSurface;
	BEGIN
		NEW(t); t.InitModel(mdl); SetName(filename$); StdInit(t);
		RETURN t
	END NewDocumentSurface;
	
	PROCEDURE (d: StdDirectory) NewDrawableSurface (mdl: Models.Model): Surface, NEW;
		VAR t: DrawableSurface;
	BEGIN
		NEW(t); t.InitModel(mdl); StdInit(t);
		RETURN t
	END NewDrawableSurface;
	*)
	
	(*
	(* Win32Surface *)

	PROCEDURE (s: Win32Surface) Create* (hdc: WinApi.HANDLE), NEW;
	BEGIN
		s.hdc := hdc;
		s.surface := LibsCairoWin32.cairo_win32_surface_create(hdc)
	END Create;
	*)
	
	(*
	PROCEDURE NewWin32Surface* (hdc: WinApi.HANDLE): Win32Surface;
		VAR s: Win32Surface;
	BEGIN
		NEW(s); s.Create(hdc);
		RETURN NIL
	END NewWin32Surface;
	*)
	
	PROCEDURE Init;
		VAR d: StdDirectory;
	BEGIN
		NEW(d); SetDir(d)
	END Init;

BEGIN
	Init
END VgStdViews.
