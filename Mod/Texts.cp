MODULE VgTexts;

	IMPORT Models := VgModels;

	CONST
	
		FONT_SLANT_NORMAL* = 0;
		FONT_SLANT_ITALIC* = 1;
		FONT_SLANT_OBLIQUE* = 2;

		FONT_WEIGHT_NORMAL* = 0;
		FONT_WEIGHT_BOLD* = 1;

		SUBPIXEL_ORDER_DEFAULT* = 0;
		SUBPIXEL_ORDER_RGB* = 1;
		SUBPIXEL_ORDER_BGR* = 2;
		SUBPIXEL_ORDER_VRGB* = 3;
		SUBPIXEL_ORDER_VBGR* = 4;

		HINT_STYLE_DEFAULT* = 0;
		HINT_STYLE_NONE* = 1;
		HINT_STYLE_SLIGHT* = 2;
		HINT_STYLE_MEDIUM* = 3;
		HINT_STYLE_FULL* = 4;

		HINT_METRICS_DEFAULT* = 0;
		HINT_METRICS_OFF* = 1;
		HINT_METRICS_ON* = 2;
	
	TYPE
		
		(* Text *)
		
		Glyph* = RECORD (* cairo_glyph_t *)
			index*: INTEGER;
			xOffset*, yOffset*: REAL
		END;
		
		TextMetrics* = RECORD (* cairo_text_extents_t *)
			xBearing*, yBearing*,
			width*, height*,
			xAdvance*, yAdvance*: REAL
		END;
		
		FontMetrics* = RECORD (* cairo_font_extents_t *)
			ascent*,
			descent*,
			height*,
			maxXadvance*, maxYadvance*: REAL
		END;
		
		HintStyle* = INTEGER; (* cairo_hint_style_t *)
		HintMetrics* = INTEGER; (* cairo_hint_metrics_t *)
		
		FontOptions* = RECORD (* _cairo_font_options *)
			antialias*: Models.Antialias;
			subPixelOrder*: Models.SubPixelOrder;
			hintStyle*: HintStyle;
			hintMetrics*: HintMetrics
		END;

		FontSlant* = INTEGER; (* cairo_font_slant_t *)
		FontWeight* = INTEGER; (* cairo_font_weight_t *)
		FontEngine* = INTEGER; (* cairo_font_type_t *)
	
END VgTexts.