MODULE VgPathsPriv;

	IMPORT TypesPriv := VgTypesPriv, Models := VgModels, Mem := StdlibMem;
(*
	FIXME Warning: StdlibMem.MemCompare is potentially dangerous!
*)
	
	CONST
		has_current_point = 0;
		has_curve_to = 1;
	
	PROCEDURE SetPoint (x, y: REAL; VAR p: Models.Point);
	BEGIN
		p[0, 0] := 0; p[1, 0] := 0
	END SetPoint;
	
	PROCEDURE ComparePoint (p1, p2: Models.Point): BOOLEAN;
	BEGIN
		RETURN (p1[0, 0] = p2[0, 0]) & (p1[1, 0] = p2[1, 0])
	END ComparePoint;
	
	PROCEDURE PathBufInit (VAR buf: TypesPriv.PathBuf; bufSize: INTEGER);
	BEGIN
		ASSERT(buf # NIL, 20);
		buf.next := NIL;
		buf.prev := NIL;
		buf.numOps := 0;
		buf.numPoints := 0;
		buf.bufSize := bufSize;
		NEW(buf.op, bufSize);
		NEW(buf.points, bufSize)
	END PathBufInit;
	
	(* _cairo_path_buf_create *)
	PROCEDURE PathBufNew* (bufSize: INTEGER): TypesPriv.PathBuf;
		VAR buf: TypesPriv.PathBuf;
	BEGIN
		(* adjust bufSize to ensure that buf->points is naturally aligned *)
		(*
		bufSize += sizeof (double)
			   - ((bufSize + sizeof (cairo_path_buf_t)) & (sizeof (double)-1));
		buf = _cairo_malloc_ab_plus_c (bufSize,
								   sizeof (cairo_path_op_t) +
					   2 * sizeof (cairo_point_t),
					   sizeof (cairo_path_buf_t));
		*)
		NEW(buf);
		IF buf # NIL THEN PathBufInit (buf, bufSize) END;
		RETURN buf
	END PathBufNew;
	
	(* _cairo_path_fixed_init *)
	PROCEDURE PathFixedInit* (VAR path: TypesPriv.PathFixed);
		VAR i: INTEGER;
	BEGIN
		path.buf_head := PathBufNew (TypesPriv.PathBufSIZE);
		path.buf_tail := path.buf_head;
		SetPoint(0, 0, path.current_point);
		path.last_move_point := path.current_point;
		path.opts := {}
	END PathFixedInit;
	
	(* _cairo_path_fixed_init *)
	PROCEDURE PathFixedFini* (VAR path: TypesPriv.PathFixed);
		VAR buf, this: TypesPriv.PathBuf;
	BEGIN
		ASSERT(path.buf_head # NIL, 20);
		buf := path.buf_head.next;
		WHILE buf # NIL DO
			this := buf;
			buf := buf.next;
			this := NIL (* _cairo_path_buf_destroy *)
		END;
		path.buf_head.next := NIL;
		path.buf_head.prev := NIL;
		path.buf_tail := path.buf_head;
		path.buf_head.numOps := 0;
		path.buf_head.numPoints := 0;
		path.opts := {}
	END PathFixedFini;
	
	(* _cairo_path_fixed_get_current_point *)
	PROCEDURE PathFixedGetPos* (path: TypesPriv.PathFixed; OUT x, y: REAL): BOOLEAN;
		VAR res: BOOLEAN;
	BEGIN
		IF ~(has_current_point IN path.opts) THEN
			x := 0; y := 0; res := FALSE
		ELSE
			SetPoint(x, y, path.current_point);
			res := TRUE
		END;
		RETURN res
	END PathFixedGetPos;
	
	PROCEDURE ComparePathOps (pathOps1, pathOps2: Models.PathOps): BOOLEAN;
		VAR res: BOOLEAN;
	BEGIN
		(*Mem.MemCompare (path_buf.op, other_buf.op,
				SIZE (Models.PathOp) * path_buf.numOps) # 0*)
		RETURN res
	END ComparePathOps;
	
	PROCEDURE ComparePoints (points1, points2: Models.Points): BOOLEAN;
		VAR res: BOOLEAN;
	BEGIN
		RETURN res
	END ComparePoints;
	
	(* _cairo_path_fixed_is_equal *)
	PROCEDURE PathFixedIsEqual* (path, other: TypesPriv.PathFixed): BOOLEAN;
		VAR path_buf, other_buf: TypesPriv.PathBuf;
	BEGIN
		IF ~ComparePoint(path.current_point, other.current_point) OR
			(path.opts # other.opts) OR
			~ComparePoint(path.last_move_point, other.last_move_point) THEN RETURN FALSE;
		END;
		other_buf := other.buf_head;
		path_buf := path.buf_head;
		WHILE path_buf # NIL DO
			IF (other_buf = NIL) OR
				(path_buf.numOps # other_buf.numOps) OR
				(path_buf.numPoints # other_buf.numPoints) OR
				~ComparePathOps(path_buf.op, other_buf.op) OR
				~ComparePoints(path_buf.points, other_buf.points) THEN RETURN FALSE;
			END;
			other_buf := other_buf.next;
			path_buf := path_buf.next
		END;
		RETURN TRUE;
	END PathFixedIsEqual;
	
	(* _cairo_path_fixed_add_buf *)
	PROCEDURE PathFixedAddBuf (VAR path: TypesPriv.PathFixed; VAR buf: TypesPriv.PathBuf);
	BEGIN
		buf.next := NIL;
		buf.prev := path.buf_tail;

		path.buf_tail.next := buf;
		path.buf_tail := buf
	END PathFixedAddBuf;
	
	(* _cairo_path_buf_add_op *)
	PROCEDURE PathFixedAddOp (buf: TypesPriv.PathBuf; op: Models.PathOp);
	BEGIN
		buf.op[buf.numOps] := op; INC(buf.numOps)
	END PathFixedAddOp;
	
	(* _cairo_path_buf_add_points *)
	PROCEDURE PathFixedAddPoints (buf: TypesPriv.PathBuf;
		points: ARRAY OF Models.Point; numPoints: INTEGER);
		VAR i: INTEGER;
	BEGIN
		FOR i := 0 TO numPoints - 1 DO
			buf.points[buf.numPoints] := points[i]; INC(buf.numPoints)
		END
	END PathFixedAddPoints;
	
	(* _cairo_path_fixed_add *)
	PROCEDURE PathFixedAdd (VAR path: TypesPriv.PathFixed; op: Models.PathOp;
		points: ARRAY OF Models.Point; numPoints: INTEGER);
		VAR buf: TypesPriv.PathBuf;
	BEGIN
		buf := path.buf_tail;
		IF (buf.numOps + 1 > buf.bufSize) OR
			(buf.numPoints + numPoints > 2 * buf.bufSize) THEN
			buf := PathBufNew (buf.bufSize * 2);
			IF buf # NIL THEN
				PathFixedAddBuf (path, buf)
			ELSE
				Models.SetStatus(Models.STATUS_NO_MEMORY); RETURN
			END
		END;
		PathFixedAddOp (buf, op);
		PathFixedAddPoints (buf, points, numPoints);
		Models.SetStatus(Models.STATUS_SUCCESS)
	END PathFixedAdd;

	(* _cairo_path_fixed_move_to *)
	PROCEDURE PathFixedMoveTo* (VAR path: TypesPriv.PathFixed; x, y: REAL);
		VAR
			last_move_to_point: Models.Point;
			point: ARRAY 1 OF Models.Point;
	BEGIN
		SetPoint(x, y, point[0]);
		INCL(path.opts, has_current_point);
		IF (path.buf_tail # NIL) & (path.buf_tail.numOps # 0) &
			(path.buf_tail.op[path.buf_tail.numOps - 1] = TypesPriv.PATH_OP_MOVE_TO) THEN
			
			last_move_to_point := path.buf_tail.points[path.buf_tail.numPoints - 1];
			last_move_to_point := point[0]
		ELSE
			PathFixedAdd (path, TypesPriv.PATH_OP_MOVE_TO, point, 1)
		END;
		Models.SetStatus(Models.STATUS_SUCCESS)
	END PathFixedMoveTo;
	
	(* _cairo_path_fixed_close_path *)
	PROCEDURE PathFixedClose* (VAR path: TypesPriv.PathFixed);
		VAR point: ARRAY 1 OF Models.Point;
	BEGIN
		IF ~(has_current_point IN path.opts) THEN
			Models.SetStatus(Models.STATUS_SUCCESS); RETURN
		END;
		PathFixedAdd(path, TypesPriv.PATH_OP_CLOSE_PATH, point, 0);
		IF Models.status # Models.STATUS_SUCCESS THEN RETURN END;
		PathFixedMoveTo(path, path.last_move_point[0, 0], path.last_move_point[1, 0]);
		Models.SetStatus(Models.STATUS_SUCCESS)
	END PathFixedClose;
	
	(* _cairo_path_fixed_new_sub_path *)
	PROCEDURE PathFixedNewSubPath* (VAR path: TypesPriv.PathFixed);
	BEGIN
		EXCL(path.opts, has_current_point)
	END PathFixedNewSubPath;
	
	(* _cairo_path_fixed_line_to *)
	PROCEDURE PathFixedLineTo* (VAR path: TypesPriv.PathFixed; x, y: REAL);
		VAR
			point: ARRAY 1 OF Models.Point;
	BEGIN
		SetPoint(x, y, point[0]);
		IF ~(has_current_point IN path.opts) THEN
			PathFixedMoveTo(path, x, y)
		ELSE
			PathFixedAdd (path, TypesPriv.PATH_OP_LINE_TO, point, 1)
		END;
		INCL(path.opts, has_current_point);
		path.current_point := point[0];
		Models.SetStatus(Models.STATUS_SUCCESS)
	END PathFixedLineTo;
	
	(* _cairo_path_fixed_curve_to *)
	PROCEDURE PathFixedCurveTo* (VAR path: TypesPriv.PathFixed;
		x0, y0,
		x1, y1,
		x2, y2: REAL);

		VAR
			point: ARRAY 3 OF Models.Point;
	BEGIN
		SetPoint(x0, y0, point[0]);
		SetPoint(x1, y1, point[1]);
		SetPoint(x2, y2, point[2]);
		IF ~(has_current_point IN path.opts) THEN
			PathFixedAdd (path, TypesPriv.PATH_OP_MOVE_TO, point, 1);
			IF Models.status # Models.STATUS_SUCCESS THEN RETURN END;
		END;
		PathFixedAdd (path, TypesPriv.PATH_OP_CURVE_TO, point, 3);
		IF Models.status # Models.STATUS_SUCCESS THEN RETURN END;
		INCL(path.opts, has_current_point);
		INCL(path.opts, has_curve_to);
		path.current_point := point[2];
		Models.SetStatus(Models.STATUS_SUCCESS)
	END PathFixedCurveTo;
	
	(*
	PROCEDURE _ ();
	BEGIN
	END ;
	*)

END VgPathsPriv.
