MODULE VgViews;
	
	IMPORT Models := VgModels;

	CONST
		STATUS_SURFACE_FINISHED* = 12;
		STATUS_SURFACE_TYPE_MISMATCH* = 13;

		ANTIALIAS_DEFAULT* = 0;
		ANTIALIAS_NONE* = 1;
		ANTIALIAS_GRAY* = 2;
		ANTIALIAS_SUBPIXEL* = 3;
	
	TYPE
		Enum = INTEGER;
		
		(* Document, drawable (screen, printer), memory buffer *)
		SurfaceType* = Enum; (* cairo_surface_type_t *)
		ImageFormat* = Enum; (* cairo_format_t *)
		
		Antialias* = Enum; (* cairo_antialias_t *)
		RasterFilter* = Enum; (* cairo_filter_t *)
		PixelOrder* = Enum;
		
		(* Device capabilities *)
		DeviceCap* = RECORD
			dpi*: INTEGER;
			xResolution*,
			yResolution*: REAL;
			bitDepth*: INTEGER;
			pixelOrder*: PixelOrder;
		END;
		
		(* Models *)

		Surface* = POINTER TO ABSTRACT RECORD (*Stores.Store*) (** View **)
			model: Models.Model;
			type-: SurfaceType;
			antialias*: Antialias;
			deviceCap-: DeviceCap;
			deviceTransform-: Models.AffineMatrix;
			(* fontOptions*: Models.FontOptions *)
		END;
		
		(** Rendering to memory buffers **)
		RasterSurface* = POINTER TO ABSTRACT RECORD (Surface)
			format: ImageFormat;
			content-: Models.ColorContent;
		END;
		
		DocumentSurface* = POINTER TO EXTENSIBLE RECORD (Surface)
			filename: ARRAY 256 OF CHAR
		END;
		
		(*
		(** Rendering to memory buffers **)
		ImageSurface* = POINTER TO EXTENSIBLE RECORD (Surface)
			format: ImageFormat;
			content-: Models.ColorContent;
		END;
		*)

		DrawableSurface* = POINTER TO EXTENSIBLE RECORD (RasterSurface)
		END;
		
		(* Directory *)
		
		Directory* = POINTER TO ABSTRACT RECORD END;
	
	VAR
		dir-, stdDir-: Directory;
	
	PROCEDURE (s: Surface) ThisModel* (): Models.Model, NEW, EXTENSIBLE;
	BEGIN
		RETURN s.model
	END ThisModel;
	
	PROCEDURE (s: Surface) InitModel* (m: Models.Model), NEW;
	BEGIN
		ASSERT((s.model = NIL) OR (s.model = m), 20);
		ASSERT(m # NIL, 21);
		s.model := m
	END InitModel;
	
	(*
	PROCEDURE (s: Surface) Stroke*, NEW, ABSTRACT;
	PROCEDURE (s: Surface) Fill*, NEW, ABSTRACT;
	PROCEDURE (s: Surface) Flush*, NEW, EMPTY;
	PROCEDURE (s: Surface) MarkDirty*, NEW, EMPTY;
	PROCEDURE (s: Surface) Paint, NEW, ABSTRACT;
	*)
	
	(* RasterSurface *)
	
	PROCEDURE (s: RasterSurface) Render* (l, t, r, b: REAL), NEW, ABSTRACT;
	
	(* ImageSurface *)
	
	(*
	PROCEDURE (s: ImageSurface) Destroy*, NEW, EMPTY;
	
	(*
	PROCEDURE (I: ImageSurface) ThisModel (): Models.Model, EXTENSIBLE;
		VAR m: Models.Model;
	BEGIN
		m := S.ThisModel^();
		IF m # NIL THEN
			RETURN m(Models.Model)
		ELSE
			RETURN NIL
		END
	END ThisModel;
	*)
	
	PROCEDURE (s: RasterSurface) Create* (format: ImageFormat), NEW, EXTENSIBLE;
	BEGIN
		s.format := format;
	END Create;
	
	PROCEDURE (s: RasterSurface) GetFormat* (): ImageFormat, NEW;
	BEGIN
		RETURN s.format
	END GetFormat;
	
	PROCEDURE (s: RasterSurface) GetContent* (): Models.ColorContent, NEW;
	BEGIN
		RETURN s.content
	END GetContent;
	*)
	
	(* DrawableSurface *)
	
	PROCEDURE (s: DrawableSurface) Destroy*, NEW, EMPTY;
	
	PROCEDURE (s: DrawableSurface) Render* (l, t, r, b: REAL), EMPTY;
	
	PROCEDURE (s: DrawableSurface) GetContent* (): Models.ColorContent, NEW;
	BEGIN
		RETURN s.content
	END GetContent;
	
	(*PROCEDURE (s: DrawableSurface) SetSize* (), NEW;*)
	
	(* DocumentSurface *)
	
	PROCEDURE (s: DocumentSurface) Destroy*, NEW, EMPTY;
	
	PROCEDURE (s: DocumentSurface) SetName* (filename: ARRAY OF CHAR), NEW, EXTENSIBLE;
	BEGIN
		s.filename := filename$
	END SetName;
	
	(*
	PROCEDURE (s: DocumentSurface) SetFallbackResolution* (x_pixels_per_inch, y_pixels_per_inch: REAL), NEW;
	BEGIN
		
	END SetFallbackResolution;
	*)
	
	(* Directory *)

	(*PROCEDURE (d: Directory) New* (mdl: Models.Model): Surface, NEW, ABSTRACT;*)
	(*PROCEDURE (d: Directory) NewImageSurface* (mdl: Models.Model; format: ImageFormat): ImageSurface, NEW, ABSTRACT;*)

	PROCEDURE SetDir* (d: Directory);
	BEGIN
		ASSERT(d # NIL, 20);
		IF stdDir = NIL THEN
			stdDir := d
		END;
		dir := d
	END SetDir;
	
END VgViews.