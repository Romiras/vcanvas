MODULE VgDemo;
	
	IMPORT Models := VgModels, StdModels := VgStdModels, Views := VgViews,
		StdLog;
	
	PROCEDURE Run*;
		VAR
			drawingModel: Models.Model;
			pattern: StdModels.SolidPattern;
			path: Models.Path;
			imgSurface, win32Surface: Views.Surface;
	BEGIN
		drawingModel := Models.dir.New(70, 50);
		ASSERT(drawingModel # NIL, 20);
		
		path := drawingModel.NewPathWriter(NIL);
		ASSERT(path # NIL, 20);
		
		path.MoveTo(50, 35, Types.ABSOLUTE);
		path.LineTo(10, 15, Types.ABSOLUTE);
		path.LineTo(20, 22, Types.ABSOLUTE);
		path.CurveTo(0, 5, 12, 43, 32, 8, Types.ABSOLUTE);
		StdLog.String("Status: "); StdLog.Int(Types.status); StdLog.Ln;
		
		(*
		path.MoveTo(25, 43, Types.ABSOLUTE);
		path.LineTo(-25, -43, Types.ABSOLUTE);
		path.LineTo(-25, 43, Types.ABSOLUTE);
		path.CurveTo(20, -15, 45, -31, -2, 8, Types.ABSOLUTE);
		path.ClosePath;
		StdLog.String("Status: "); StdLog.Int(Types.status); StdLog.Ln;
		*)
		
		pattern := StdModels.NewSolidPattern() (StdModels.SolidPattern);
		ASSERT(pattern # NIL, 21);
		pattern.SetColorRGB(0.8, 0.6, 0.24);
		
		drawingModel.SetLineWidth(0.2);
		drawingModel.SetSource(pattern);
		
		imgSurface := Views.dir.NewImageSurface(drawingModel, Types.FORMAT_RGB24);
		ASSERT(imgSurface # NIL, 21);
		imgSurface.Fill;
		imgSurface.Destroy;
		
		win32Surface := Views.NewWin32Surface(drawingModel);
		ASSERT(win32Surface # NIL, 21);
		win32Surface.Fill;
		win32Surface.Destroy;
		
		drawingModel.Destroy
	END Run;
	
END VgDemo.


VgDemo VgViews VgStdModels VgStates VgPaths VgUtils VgTypesPriv VgModels VgTypes StdlibMem

VgDemo.Run