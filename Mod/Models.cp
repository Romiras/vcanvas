MODULE VgModels;

	CONST
		STATUS_SUCCESS* = 0;
		STATUS_NO_MEMORY* = 1;
		STATUS_INVALID_RESTORE* = 2;
		STATUS_INVALID_POP_GROUP* = 3;
		STATUS_NO_CURRENT_POINT* = 4;
		STATUS_INVALID_MATRIX* = 5;
		STATUS_INVALID_STATUS* = 6;
		STATUS_NULL_POINTER* = 7;
		STATUS_INVALID_STRING* = 8;
		STATUS_INVALID_PATH_DATA* = 9;
		STATUS_READ_ERROR* = 10;
		STATUS_WRITE_ERROR* = 11;
		STATUS_PATTERN_TYPE_MISMATCH* = 14;
		STATUS_INVALID_CONTENT* = 15;
		STATUS_INVALID_FORMAT* = 16;
		STATUS_INVALID_VISUAL* = 17;
		STATUS_FILE_NOT_FOUND* = 18;
		STATUS_INVALID_DASH* = 19;

		ABSOLUTE* = 0;
		RELATIVE* = 1;

		FILL_RULE_WINDING* = 0;
		FILL_RULE_EVEN_ODD* = 1;

		LINE_CAP_BUTT* = 0;
		LINE_CAP_ROUND* = 1;
		LINE_CAP_SQUARE* = 2;

		LINE_JOIN_MITER* = 0;
		LINE_JOIN_ROUND* = 1;
		LINE_JOIN_BEVEL* = 2;

		EXTEND_NONE* = 0;
		EXTEND_REPEAT* = 1;
		EXTEND_REFLECT* = 2;
		EXTEND_PAD* = 3;

		FILTER_FAST* = 0;
		FILTER_GOOD* = 1;
		FILTER_BEST* = 2;
		FILTER_NEAREST* = 3;
		FILTER_BILINEAR* = 4;
		FILTER_GAUSSIAN* = 5;

		PATTERN_TYPE_SOLID* = 0;
		PATTERN_TYPE_SURFACE* = 1;
		PATTERN_TYPE_LINEAR* = 2;
		PATTERN_TYPE_RADIAL* = 3;

		OPERATOR_CLEAR* = 0;
		OPERATOR_SOURCE* = 1;
		OPERATOR_OVER* = 2;
		OPERATOR_IN* = 3;
		OPERATOR_OUT* = 4;
		OPERATOR_ATOP* = 5;
		OPERATOR_DEST* = 6;
		OPERATOR_DEST_OVER* = 7;
		OPERATOR_DEST_IN* = 8;
		OPERATOR_DEST_OUT* = 9;
		OPERATOR_DEST_ATOP* = 10;
		OPERATOR_XOR* = 11;
		OPERATOR_ADD* = 12;
		OPERATOR_SATURATE* = 13;

		CONTENT_COLOR* = 1000H;
		CONTENT_ALPHA* = 2000H;
		CONTENT_COLOR_ALPHA* = 3000H;
	
	TYPE
		RealArray* = POINTER TO ARRAY OF REAL;
		Enum* = INTEGER;
		Status* = Enum;
		
		Vector2D* = ARRAY 2 OF REAL; (* Raw vector 1x2 *)
		
		Point* = Vector2D;
		TransformMatrix* = ARRAY 2, 2 OF REAL;
		TranslationMatrix* = Vector2D;
		
		AffineMatrix* = RECORD (* cairo_matrix_t *)
			tf* : TransformMatrix;
			tl* : TranslationMatrix
		END;
		
		PathType* = Enum; (* cairo_path_data_type_t *)
		
		PathPoint* = Point; (* cairo_path_data_type_t *)
		
		PathOp* = Enum; (* cairo_path_op *)
		
		Dashes* = RealArray;
		Points* = POINTER TO ARRAY OF Point;
		PathOps* = POINTER TO ARRAY OF PathOp;

		FillRule* = Enum; (* cairo_fill_rule_t *)
		LineCap* = Enum; (* cairo_line_cap_t *)
		LineJoin* = Enum; (* cairo_line_join_t *)
		
		StrokeStyle* = RECORD (* _cairo_stroke_style *)
			lineWidth* : REAL;
			lineCap* : LineCap;
			lineJoin* : LineJoin;
			numDashes* : INTEGER;
			dash* : Dashes;
			dashOffset*,
			miterLimit* : REAL
		END;
		
		PatternType* = Enum; (* cairo_pattern_type_t *)
		PatternExtend* = Enum; (* cairo_extend_t *)
		Operator* = Enum; (* cairo_operator_t *)
		
		Store* = POINTER TO ABSTRACT RECORD END;
		
		ColorContent* = Enum; (* cairo_content_t *)
		
		(* Models *)
		
		Pattern* = POINTER TO ABSTRACT RECORD
			type-: PatternType;
			extend-: PatternExtend;
			matrix-: AffineMatrix;
		END;
		
		Model* = POINTER TO ABSTRACT RECORD
			width-, height- : REAL
		END;
		
		Path* = POINTER TO ABSTRACT  RECORD END;
		
		(* Directory *)
		
		Directory* = POINTER TO ABSTRACT RECORD END;
	
	VAR
		dir-, stdDir-: Directory;
		status-: Status;
		
	PROCEDURE SetStatus* (stat: Status);
	BEGIN
		status := stat
	END SetStatus;
	
	(* Store *)
	
	(*PROCEDURE (s: Store) Internalize- (VAR rd: Reader), NEW, EMPTY;
	PROCEDURE (s: Store) Externalize- (VAR wr: Writer), NEW, EMPTY;*)
	
	(* Context *)
	(*
	PROCEDURE (c: Context) ThisModel* (): Model, NEW;
		VAR m: Model;
	BEGIN
		
		RETURN m
	END ThisModel;
	*)
	
	(* Model *)
	
	PROCEDURE (M: Model) Destroy*, NEW, ABSTRACT;

	PROCEDURE (M: Model) SetSize*(w, h: REAL), NEW;
	BEGIN
		M.width := w; M.height := h
	END SetSize;

	PROCEDURE (M: Model) GetStatus*(): Status, NEW;
	BEGIN
		RETURN status
	END GetStatus;
	
	PROCEDURE (M: Model) Translate* (tx, ty: REAL), NEW, ABSTRACT;
	PROCEDURE (M: Model) Scale* (sx, sy: REAL), NEW, ABSTRACT;
	PROCEDURE (M: Model) Rotate* (angle: REAL), NEW, ABSTRACT;
	PROCEDURE (M: Model) Transform* (matrix: AffineMatrix), NEW, ABSTRACT;
	
	PROCEDURE (M: Model) NewPathWriter* (old: Path): Path, NEW, ABSTRACT;
	
	PROCEDURE (M: Model) Save*, NEW, ABSTRACT;
	PROCEDURE (M: Model) Restore*, NEW, ABSTRACT;
	PROCEDURE (M: Model) PushGroup*, NEW, ABSTRACT;
	PROCEDURE (M: Model) PopGroup*, NEW, ABSTRACT;
	
	PROCEDURE (M: Model) GetExtents* (OUT x1, y1, x2, y2: REAL), NEW, ABSTRACT;
	PROCEDURE (M: Model) SetLineWidth* (width: REAL), NEW, ABSTRACT;
	PROCEDURE (M: Model) GetLineWidth* (): REAL, NEW, ABSTRACT;
	PROCEDURE (M: Model) SetLineCap* (line_cap: LineCap), NEW, ABSTRACT;
	PROCEDURE (M: Model) GetLineCap* (): LineCap, NEW, ABSTRACT;
	PROCEDURE (M: Model) SetLineJoin* (line_join: LineJoin), NEW, ABSTRACT;
	PROCEDURE (M: Model) GetLineJoin* (): LineJoin, NEW, ABSTRACT;
	PROCEDURE (M: Model) SetMiterLimit* (limit: REAL), NEW, ABSTRACT;
	PROCEDURE (M: Model) GetMiterLimit* (): REAL, NEW, ABSTRACT;
	PROCEDURE (M: Model) SetDash* (dashes: Dashes; num_dashes: INTEGER; offset: REAL), NEW, ABSTRACT;
	PROCEDURE (M: Model) GetDashCount* (): INTEGER, NEW, ABSTRACT;
	PROCEDURE (M: Model) GetDash* (OUT dashes: Dashes; VAR offset: REAL), NEW, ABSTRACT;
	PROCEDURE (M: Model) SetFillRule* (fill_rule: FillRule), NEW, ABSTRACT;
	PROCEDURE (M: Model) GetFillRule* (): FillRule, NEW, ABSTRACT;
	PROCEDURE (M: Model) SetSource* (source: Pattern), NEW, ABSTRACT;
	PROCEDURE (M: Model) GetSource* (): Pattern, NEW, ABSTRACT;
	PROCEDURE (M: Model) SetMask* (source: Pattern), NEW, ABSTRACT;
	PROCEDURE (M: Model) SetOperator* (op: Operator), NEW, ABSTRACT;
	PROCEDURE (M: Model) GetOperator*  (): Operator, NEW, ABSTRACT;
	PROCEDURE (M: Model) PathGetNumPoints* (): INTEGER, NEW, ABSTRACT;
	PROCEDURE (M: Model) PathGetPoints* (OUT points: Points), NEW, ABSTRACT;
	PROCEDURE (M: Model) PathGetOp* (OUT ops: PathOps), NEW, ABSTRACT;
	PROCEDURE (M: Model) PathGetFirst* (): BOOLEAN, NEW, ABSTRACT;
	PROCEDURE (M: Model) PathGetNext* (): BOOLEAN, NEW, ABSTRACT;
	
	(* Path (writer) *)

	PROCEDURE (f: Path) Base*(): Model, NEW, ABSTRACT;	
	PROCEDURE (f: Path) NewPath*, NEW, ABSTRACT;
	PROCEDURE (f: Path) NewSubPath*, NEW, ABSTRACT;
	PROCEDURE (f: Path) ClosePath*, NEW, ABSTRACT;
	PROCEDURE (f: Path) AppendPath* (path: Path), NEW, ABSTRACT;
	PROCEDURE (f: Path) CopyPath* (): Path, NEW, ABSTRACT;
	PROCEDURE (f: Path) MoveTo* (x, y: REAL; mode: Enum), NEW, ABSTRACT;
	PROCEDURE (f: Path) GetCurrentPoint* (OUT x, y: REAL), NEW, ABSTRACT;
	PROCEDURE (f: Path) LineTo*(x, y: REAL; mode: Enum), NEW, ABSTRACT;
	PROCEDURE (f: Path) CurveTo*(x1, y1, x2, y2, x3, y3: REAL; mode: Enum), NEW, ABSTRACT;

	(* Pattern *)

	(*PROCEDURE (p: Pattern) InitPattern* (type: Types.PatternType), NEW, ABSTRACT;*)
	
	PROCEDURE (p: Pattern) SetType*(type: PatternType), NEW;
	BEGIN
		p.type := type
	END SetType;
	
	PROCEDURE (p: Pattern) SetExtend*(extend: PatternExtend), NEW;
	BEGIN
		p.extend := extend
	END SetExtend;
	
	(*
	PROCEDURE (p: Pattern) SetFilter*(filter: RasterFilter), NEW;
	BEGIN
		p.filter := filter
	END SetFilter;
	*)
	
	PROCEDURE (p: Pattern) SetMatrix*(matrix: AffineMatrix), NEW;
	BEGIN
		p.matrix := matrix
	END SetMatrix;
	
	(* View *)
	
	(* Directory *)

	PROCEDURE (d: Directory) New* (w, h: REAL): Model, NEW, ABSTRACT;

	PROCEDURE SetDir* (d: Directory);
	BEGIN
		ASSERT(d # NIL, 20);
		IF stdDir = NIL THEN
			stdDir := d
		END;
		dir := d
	END SetDir;
	
END VgModels.