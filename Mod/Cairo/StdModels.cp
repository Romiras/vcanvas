MODULE VgStdModels;

	(* Implementation by Cairo graphics library *)

	IMPORT
		TypesPriv := VgTypesPriv, Models := VgModels,
		States := VgStatesPriv, Paths := VgPathsPriv, Utils := VgUtils,
		Cairo := LibsCairo,
		Log := StdLog;
	
	TYPE
		
		Point = Models.Point;
		(*Rectangle = Models.Rectangle;*)
		
		(*
		FontFace = RECORD (* cairo_font_face_t *)
		END;
		
		ScaledFont = RECORD (* cairo_scaled_font_t *)
		END;
		*)
		
		(*
		State = POINTER TO RECORD (* _cairo_gstate *)
			strokeStyle: StrokeStyle;
			fillRule: Models.FillRule;
			source: Models.Pattern;

			op: Models.Operator;
			clip: Clip;
			tolerance: REAL;
			antialias: Models.Antialias;
			
			(*
			fontFace: FontFace; (* ptr *)
			scaledFont: ScaledFont; (* ptr *)
			fontMatrix: Models.AffineMatrix;
			fontOptions: Models.FontOptions;
			
			target,
			parent_target,
			original_target: Models.View; (* ptr *)
			*)
			
			ctm,
			ctm_inverse,
			source_ctm_inverse: Models.AffineMatrix;
			
			next: State
		END;
		*)
		

		(* Implementation *)

		StdDirectory = POINTER TO RECORD (Models.Directory) END;
		
		StdModel = POINTER TO RECORD (Models.Model)
			(*ctx: Context;
			store: Store;*)
			(* attr: ModelAttributes; *)
			path: TypesPriv.PathFixed;
			state, stateTail: TypesPriv.ModelState;
			
			(*
			userData: Array;
			gstate: State;
			gstateTail: State;
			*)
		END;
		
		StdPath = POINTER TO RECORD (Models.Path)
			(*status: Models.Status;*)
			base: StdModel;	(* base = Base() *)
			numData: INTEGER;
			data: TypesPriv.PathData;
		END;

		StdPattern = POINTER TO ABSTRACT RECORD (Models.Pattern)
			pattern: Cairo.Pcairo_pattern_t;
			(*userData: Array*)
		END;

		(*StdContext = POINTER TO RECORD (Models.Context)
			text: StdModel;
			ref: ViewRef
		END;*)

		(* Public models *)
		
		(* cairo_solid_pattern_t *)
		SolidPattern* = POINTER TO RECORD (StdPattern)
			color-: Models.RGBColor;
			content: Models.ColorContent
		END;

		GradientStop* = POINTER TO RECORD (* cairo_gradient_stop_t *)
			offset-: REAL;
			color-: Models.RGBColor;
			next-: GradientStop
		END;
		
		(* cairo_gradient_pattern_t *)
		GradientPattern* = POINTER TO ABSTRACT RECORD (StdPattern)
			tailStop: GradientStop;
			stops-: GradientStop
		END;
		
		(* cairo_linear_pattern_t *)
		LinearPattern* = POINTER TO RECORD (GradientPattern)
			p1-, p2-: Models.Point
		END;
		
		(* cairo_radial_pattern_t *)
		RadialPattern* = POINTER TO RECORD (GradientPattern)
			r1-: INTEGER;
			c1-: Models.Point;
			r2-: INTEGER;
			c2-: Models.Point
		END;
		
		(*
		(* cairo_surface_pattern_t *)
		SurfacePattern* = POINTER TO RECORD (StdPattern)
			surface: Views.Surface (* ptr *)
		END;
		*)
	
	VAR
		head: TypesPriv.PathBuf;

	(*
	PROCEDURE UserDataArrayInit (userData: Array);
	BEGIN
		(*
		array->size = 0;
		array->num_elements = 0;
		array->element_size = sizeof (cairo_user_data_slot_t);
		array->elements = NULL;
		array->is_snapshot = FALSE;
		*)
	END UserDataArrayInit;
	*)
	
	PROCEDURE StdInit (t: StdModel);
		VAR
	BEGIN
		ASSERT(t # NIL, 20); ASSERT(t.state # NIL, 21);
		Models.SetStatus(Models.STATUS_SUCCESS);
		
		States.StateInit (t.state);
		t.stateTail := t.state;
		(*UserDataArrayInit(t.userData);*)
		
		Paths.PathFixedInit(t.path)
	END StdInit;

	(* StdModel *)
	
	PROCEDURE (M: StdModel) Destroy;
	BEGIN
		Log.String("Destroy"); Log.Ln;

		WHILE (M.state # M.stateTail) & (Models.status = Models.STATUS_SUCCESS)  DO
			States.StateRestore(M.state)
		END;
		States.StateFini (M.state);
		Paths.PathFixedFini(M.path);
		(*_cairo_user_data_array_fini (&cr->user_data);*)
	END Destroy;
	
	PROCEDURE (M: StdModel) Translate (tx, ty: REAL);
	BEGIN
		States.Translate(M.state, tx, ty)
	END Translate;
	
	PROCEDURE (M: StdModel) Scale (sx, sy: REAL);
	BEGIN
		States.Scale(M.state, sx, sy)
	END Scale;
	
	PROCEDURE (M: StdModel) Rotate (angle: REAL);
	BEGIN
		States.Rotate(M.state, angle)
	END Rotate;
	
	PROCEDURE (M: StdModel) Transform (matrix: Models.AffineMatrix);
	BEGIN
		States.Transform(M.state, matrix)
	END Transform;
	
	PROCEDURE (M: StdModel) Save;
	BEGIN
		States.StateSave(M.state)
	END Save;
	
	PROCEDURE (M: StdModel) Restore;
	BEGIN
		States.StateRestore(M.state)
	END Restore;
	
	PROCEDURE (M: StdModel) PushGroup;
	BEGIN
	END PushGroup;
	
	PROCEDURE (M: StdModel) PopGroup;
	BEGIN
	END PopGroup;
	
	(*
	PROCEDURE (M: StdModel) GetUserData;
	BEGIN
	END GetUserData;
	
	PROCEDURE (M: StdModel) SetUserData;
	BEGIN
	END SetUserData;
	*)

	PROCEDURE (M: StdModel) NewPathWriter (old: Models.Path): Models.Path;
		VAR wr: StdPath;
	BEGIN
		IF (old # NIL) & (old IS StdPath) THEN
			wr := old(StdPath)
		ELSE
			NEW(wr)
		END;
		wr.base := M;
		RETURN wr
	END NewPathWriter;
	
	PROCEDURE (M: StdModel) GetExtents (OUT x1, y1, x2, y2: REAL);
	BEGIN
	END GetExtents;
	
	PROCEDURE (M: StdModel) SetLineWidth (width: REAL);
	BEGIN
		M.state.attr.strokeStyle.lineWidth := width
	END SetLineWidth;
	
	PROCEDURE (M: StdModel) GetLineWidth (): REAL;
	BEGIN
		RETURN M.state.attr.strokeStyle.lineWidth
	END GetLineWidth;
	
	PROCEDURE (M: StdModel) SetLineCap (lineCap: Models.LineCap);
	BEGIN
		M.state.attr.strokeStyle.lineCap := lineCap
	END SetLineCap;
	
	PROCEDURE (M: StdModel) GetLineCap (): Models.LineCap;
	BEGIN
		RETURN M.state.attr.strokeStyle.lineCap
	END GetLineCap;
	
	PROCEDURE (M: StdModel) SetLineJoin (lineJoin: Models.LineJoin);
	BEGIN
		M.state.attr.strokeStyle.lineJoin := lineJoin
	END SetLineJoin;
	
	PROCEDURE (M: StdModel) GetLineJoin (): Models.LineJoin;
	BEGIN
		RETURN M.state.attr.strokeStyle.lineJoin
	END GetLineJoin;
	
	PROCEDURE (M: StdModel) SetMiterLimit (limit: REAL);
	BEGIN
		M.state.attr.strokeStyle.miterLimit := limit
	END SetMiterLimit;
	
	PROCEDURE (M: StdModel) GetMiterLimit (): REAL;
	BEGIN
		RETURN M.state.attr.strokeStyle.miterLimit
	END GetMiterLimit;
	
	PROCEDURE (M: StdModel) SetDash (dashes: Models.Dashes; numDashes: INTEGER; offset: REAL);
	BEGIN
		M.state.attr.strokeStyle.numDashes := numDashes;
		M.state.attr.strokeStyle.dash := dashes;
		M.state.attr.strokeStyle.dashOffset := offset
	END SetDash;
	
	PROCEDURE (M: StdModel) GetDashCount (): INTEGER;
	BEGIN
		RETURN M.state.attr.strokeStyle.numDashes
	END GetDashCount;
	
	PROCEDURE (M: StdModel) GetDash (OUT dashes: Models.Dashes; VAR offset: REAL);
	BEGIN
		dashes := M.state.attr.strokeStyle.dash;
		offset := M.state.attr.strokeStyle.dashOffset
	END GetDash;
	
	PROCEDURE (M: StdModel) SetFillRule (fillRule: Models.FillRule);
	BEGIN
		M.state.attr.fillRule := fillRule
	END SetFillRule;
	
	PROCEDURE (M: StdModel) GetFillRule (): Models.FillRule;
	BEGIN
		RETURN M.state.attr.fillRule
	END GetFillRule;

	PROCEDURE (M: StdModel) SetOperator (op: Models.Operator);
	BEGIN
		M.state.attr.operator := op
	END SetOperator;
	
	PROCEDURE (M: StdModel) GetOperator  (): Models.Operator;
	BEGIN
		RETURN M.state.attr.operator
	END GetOperator;
	
	(* _cairo_gstate_set_source *)
	PROCEDURE (M: StdModel) SetSource (source: Models.Pattern);
	BEGIN
		ASSERT(source # NIL);
		IF Models.status = Models.STATUS_SUCCESS THEN
			(* cairo_pattern_destroy (gstate->source); *)
			M.state.attr.source := source
		END
	END SetSource;
	
	PROCEDURE (M: StdModel) GetSource (): Models.Pattern;
	BEGIN
		IF Models.status = Models.STATUS_SUCCESS THEN
			RETURN M.state.attr.source
		ELSE
			RETURN NIL
		END
	END GetSource;
	
	PROCEDURE (M: StdModel) SetMask (source: Models.Pattern);
	BEGIN
	END SetMask;
	
	PROCEDURE (M: StdModel) PathGetNumPoints (): INTEGER;
	BEGIN
		RETURN M.path.buf_head.numPoints
	END PathGetNumPoints;
	
	PROCEDURE (M: StdModel) PathGetPoints (OUT points: POINTER TO ARRAY OF Models.Point);
	BEGIN
		points := M.path.buf_head.points
	END PathGetPoints;
	
	PROCEDURE (M: StdModel) PathGetOp (OUT ops: POINTER TO ARRAY OF Models.PathOp);
	BEGIN
		ops :=M.path.buf_head.op 
	END PathGetOp;
	
	PROCEDURE (M: StdModel) PathGetFirst (): BOOLEAN;
	BEGIN
		head := M.path.buf_head; RETURN head # NIL
	END PathGetFirst;
	
	PROCEDURE (M: StdModel) PathGetNext (): BOOLEAN;
	BEGIN
		IF head = NIL THEN
			RETURN FALSE
		ELSE
			head := head.next;
			RETURN head # NIL
		END
	END PathGetNext;
	

	(* StdPath *)
	
	PROCEDURE (p: StdPath) Base (): Models.Model;
	BEGIN
		RETURN p.base
	END Base;
	
	PROCEDURE (p: StdPath) NewPath;
	BEGIN
		IF Models.status # Models.STATUS_SUCCESS THEN RETURN END;
		Paths.PathFixedFini(p.base.path)
	END NewPath;
	
	PROCEDURE (p: StdPath) NewSubPath;
	BEGIN
		Paths.PathFixedNewSubPath(p.base.path)
	END NewSubPath;
	
	PROCEDURE (p: StdPath) ClosePath;
	BEGIN
		Paths.PathFixedClose(p.base.path)
	END ClosePath;
	
	PROCEDURE (p: StdPath) AppendPath (path: Models.Path);
	BEGIN
	END AppendPath;
	
	PROCEDURE (p: StdPath) CopyPath (): Models.Path;
		VAR f: Models.Path;
	BEGIN
		
		RETURN f
	END CopyPath;
	
	PROCEDURE (p: StdPath) GetCurrentPoint (OUT x, y: REAL);
	BEGIN
		IF Paths.PathFixedGetPos(p.base.path, x, y) THEN
			Models.SetStatus(Models.STATUS_SUCCESS)
		ELSE
			Models.SetStatus(Models.STATUS_NO_CURRENT_POINT)
		END
	END GetCurrentPoint;
	
	PROCEDURE (p: StdPath) MoveTo (x, y: REAL; mode: INTEGER);
		VAR xx, yy: REAL;
	BEGIN
		xx := x;
		yy := y;
		IF mode = Models.RELATIVE THEN
			p.GetCurrentPoint(x, y);
			IF Models.status # Models.STATUS_SUCCESS THEN RETURN END;
			xx := xx + x;
			yy := yy + y;
		END;
		Paths.PathFixedMoveTo(p.base.path, xx, yy)
	END MoveTo;
	
	PROCEDURE (p: StdPath) LineTo (x, y: REAL; mode: INTEGER);
	BEGIN
		Paths.PathFixedLineTo(p.base.path, x, y)
	END LineTo;
	
	PROCEDURE (p: StdPath) CurveTo (x1, y1, x2, y2, x3, y3: REAL; mode: INTEGER);
	BEGIN
		Paths.PathFixedCurveTo(p.base.path, x1, y1, x2, y2, x3, y3)
	END CurveTo;
	
	(* StdPattern *)

	PROCEDURE PatternInit (pat: StdPattern; type: Models.PatternType);
		VAR matrix: Models.AffineMatrix;
	BEGIN
		pat.SetType(type);
		(*UserDataArrayInit(pat.userData);*)
		
		IF pat.type = Models.PATTERN_TYPE_SURFACE THEN
			pat.SetExtend(TypesPriv.EXTEND_SURFACE_DEFAULT)
		ELSE
			pat.SetExtend(TypesPriv.EXTEND_GRADIENT_DEFAULT)
		END;
		Utils.MatrixInitIdentity(matrix);
		pat.SetMatrix(matrix);
		pat.SetFilter(TypesPriv.FILTER_DEFAULT)
	END PatternInit;
	
	(*PROCEDURE (pat: StdPattern) Transform (matrix: Models.AffineMatrix), NEW;
	BEGIN
	END Transform;*)

	(* SolidPattern *)
	
	PROCEDURE (P: SolidPattern) SetColor* (red, green, blue: REAL), NEW;
		VAR color: Models.RGBColor;
	BEGIN
		IF Models.status # Models.STATUS_SUCCESS THEN RETURN END;

		(* use cached or create new *)
		
		Utils.RestrictValue(red, 0.0, 1.0);
		Utils.RestrictValue(green, 0.0, 1.0);
		Utils.RestrictValue(blue, 0.0, 1.0);
		
		P.color.SetColor(red, green, blue);
		IF color.alpha = 1.0 THEN
			P.content := Models.CONTENT_COLOR
		ELSE
			P.content := Models.CONTENT_COLOR_ALPHA
		END;
		
		PatternInit(P, Models.PATTERN_TYPE_SOLID)
	END SetColor;
	
	PROCEDURE (P: SolidPattern) SetAlpha* (alpha: REAL), NEW;
	BEGIN
		IF Models.status # Models.STATUS_SUCCESS THEN RETURN END;

		(* use cached or create new *)
		
		Utils.RestrictValue(alpha, 0.0, 1.0);
		P.color.SetAlpha(alpha)
	END SetAlpha;
	
	(*
	PROCEDURE (P: SolidPattern) SetSurface (surface: Views.Surface; x, y: REAL), NEW;
		VAR matrix: Models.AffineMatrix;
	BEGIN
		Utils.MatrixInitTranslate(matrix, -x, -y);
		P.SetMatrix(matrix);
	END SetSurface;
	*)

	(* CrGradientPattern *)
	
	PROCEDURE (ptn: GradientPattern) AddColorStop* (color: Models.RGBColor; offset: REAL), NEW;
		VAR
			newStop: GradientStop;
	BEGIN
		NEW(newStop);
		IF newStop # NIL THEN
			newStop^.next := NIL;
			newStop^.color.SetColor(color.red, color.green, color.blue);
			newStop^.color.SetAlpha(color.alpha);
			newStop^.offset := offset;
			ptn.tailStop^.next := newStop;
			ptn.tailStop := newStop;
			Models.SetStatus(Models.STATUS_SUCCESS)
		ELSE
			Models.SetStatus(Models.STATUS_NO_MEMORY)
		END
	END AddColorStop;
	
	(* New patterns *)
	
	PROCEDURE NewSolidPattern* (): Models.Pattern;
		VAR t: SolidPattern;
	BEGIN
		NEW(t); RETURN t
	END NewSolidPattern;

	PROCEDURE NewLinearPattern* (): Models.Pattern;
		VAR t: LinearPattern;
	BEGIN
		NEW(t); RETURN t
	END NewLinearPattern;
	
	PROCEDURE NewRadialPattern* (): Models.Pattern;
		VAR t: RadialPattern;
	BEGIN
		NEW(t); RETURN t
	END NewRadialPattern;
	
	(* StdDirectory *)
	
	PROCEDURE (d: StdDirectory) New (w, h: REAL): Models.Model;
		VAR t: StdModel;
	BEGIN
		NEW(t); NEW(t.state); StdInit(t); t.SetSize(w, h); head := NIL;
		RETURN t
	END New;
	
	PROCEDURE Init;
		VAR d: StdDirectory;
	BEGIN
		NEW(d); Models.SetDir(d)
	END Init;

BEGIN
	Init
END VgStdModels.
